#!/usr/bin/python3
import json
import os
import time

import requests

ddns = ""  # The dns record we want to update on Cloudflare.

zone_identifier = ""  # your Zone ID

# zone id for the dns record we want to update.
# (find your dns id on dnslist => https://api.cloudflare.com/#dns-records-for-a-zone-list-dns-records)
dns_id = ""

email = "" # your registered email address on cloudflare
authkey = ""  # your X-Auth-Key

# Location of this script.
dir_path = os.getcwd()


def log(text, error=False):
    file = open(dir_path + "/ddns.log", 'a')
    ltype = "ERROR: " if error else ""
    file.write("{}{}\n\n".format(ltype, text))
    file.close()


if __name__ == '__main__':
    while True:
        try:
            # Discover your DNS IP address.
            req_ip = requests.get(
                "https://api.cloudflare.com/client/v4/zones/{}/dns_records/{}".format(zone_identifier, dns_id),
                headers={"X-Auth-Email": email, "X-Auth-Key": authkey, "Content-Type": "application/json"})
            ip = req_ip.json()['result']['content']

            # Discover your public IP address.
            my_ip = requests.get("https://api.ipify.org?format=json").json()['ip']

            if ip != my_ip:
                data = {"type": "A", "name": ddns, "content": my_ip}
                # Update the record
                req = requests.put(
                    "https://api.cloudflare.com/client/v4/zones/{}/dns_records/{}".format(zone_identifier, dns_id),
                    data=json.dumps(data), headers={"X-Auth-Email": email, "X-Auth-Key": authkey,
                                                    "Content-Type": "application/json"})
                log('DNS ip address has been changed')
        except Exception as e:
            log(e, error=True)

        time.sleep(60)
